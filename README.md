#T-Geo Project 

Hey otman (hope i spelled it right),  below i have given you instructions on how to integrate the template into your project. 

Downloading the Project: 

 - If git is not installed, install from http://git-scm.com/downloads
 - To download the project type the following command in the command prompt or terminal if in linux
       

    **git clone https://arj-196@bitbucket.org/arj-196/tgeo.git**

 



First, you will not need any other css files, i have included everything that is required. 

There are two files that you will use:

 - login.html : login page
 - display_1.php : main platform page
 
**Explanations of files: **

**Login.html**
   This page is very simple, you just need to change the form variables. 

**display_1.php**
Here you need to edit the code inside the for loops. I don't think any information is missing, so all you need to do is add your php variables in the corresponding tags 

example:

      <?php foreach($tweets as $tweet) { //$tweet is an array of tweets ?> 
        <!--stories-->
        <div class="row">    
          <div class="col-sm-9">
          <h4><span class="glyphicon glyphicon-map-marker"></span> Paris</h4>
          <span class="font-large">
                 <?php $tweet['text'] 
                          <a target="_blank"
                               href="<?php $tweet['link'] ?>"><?php $tweet['link'] ?></a>
                                  </span>
                                                                  
                          <h4><small class="text-muted"> 
                          <?php $tweet['createdAt'] ?></small></h4>
          </div>
          <div class="col-sm-3">
                 <div class="col-sm-12">
                     <a href="#" class="col-sm-1 col-sm-offset-3">
                        <img src="<?php $tweet['user_img'] ?>" class="img-circle">
                     </a>
                 </div>
                 <div class="col-sm-12">
                     <table class="info col-sm-3 col-sm-offset-1">
                         <tr>  
                           <td class="padding-right text-right"> 
                             <?php $tweet['abonnees'] ?>
                           </td>   
                           <td class="bold text-left">Abonnés</td><
                         /tr>
                         <tr>
                           <td class="padding-right text-right">
                             <?php $tweet['tweets'] ?> 
                            </td>  
                           <td class="bold text-left">Tweets </td>
                         </tr>
                         <tr>  
                           <td class="padding-right text-right">
                             <?php $tweet['amis'] ?> 
                           </td>   
                           <td class="bold text-left">Amis</td>    
                         </tr>
                         <tr>  
                           <td class="padding-right text-right">
                             <?php $tweet['liste'] ?> 
                           </td>     
                           <td class="bold text-left">Listé </td> 
                          </tr>
                     </table>
                 </div>
          </div>
               <a class="vendor" href=""> Google</a>
                 
                <div class="col-sm-12 action">
                    <span>Was this relevant? </span><br>
                    <a href="<?php $tweet[''] ?>">
                     <i class="fa fa-check fa-2x"></i><div class="space-5"></div>
                    </a>
                    <a  href="<?php $tweet[''] ?>">
                     <i class="fa fa-times fa-2x"></i><div class="space-5"></div>
                    </a>
                    <a  href="<?php $tweet[''] ?>">
                     <i class="fa fa-reply fa-2x"></i><div class="space-5"></div>
                    </a>
                </div>
        </div>
      
               <div class="row divider">    
             <div class="col-sm-12"><hr>
           </div>
        </div>    
        <!--/stories-->
      <?php  } ?>


Hope it was clear for you. 


Cheers 