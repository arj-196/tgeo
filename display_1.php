<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Basis Template for Bootstrap 3</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
		<div class="wrapper">
            
		<div class="box">

        <nav class="navbar navbar-default header">
          <div class="container-fluid" style="padding:0px;">
            <div class="navbar-header">
              <a class="navbar-brand brand-logo" href="#" >
                <img alt="Brand" src="assets/logo_orange.png">
              </a>
            </div>
              <p class="navbar-text  large-text" style="color:white">T-GEO</p>
          </div>
        </nav>
            
			<div class="row" style="padding-right:10px;">
			  
                <!-- Left Panel -->
				<div class="column main col-sm-6 column-left" >
					<div class="padding">
						<div class="full col-sm-9">
						  
							<!-- content -->
							
							<div class="col-sm-12" id="featured">   
							  <div class="page-header text-muted">
							  Villes fibrées 
							  </div> 
							</div>
							
                            <?php for($i=0;$i<20;$i++) { ?>
							<!--stories-->
							<div class="row">    
							  <div class="col-sm-9">
								<h4><span class="glyphicon glyphicon-map-marker"></span> Paris</h4>
								<span class="font-large">
                                    Eau : plus de coupures, mais débit réduit: SOSconso. L'interdiction ou pas des 
                                    coupures d'eau a donné lieu à u... 
                                    <a target="_blank" href="http://t.co/8pNjFbgAuS">http://t.co/8pNjFbgAuS</a>
                                  </span>
                                                                  
                                <h4><small class="text-muted"> 18:20 - 20 Avril 2015 </small></h4>
							  </div>
							  <div class="col-sm-3">
                                  <div class="col-sm-12">
                                      <a href="#" class="col-sm-1 col-sm-offset-3">
                                          <img src="http://api.randomuser.me/portraits/thumb/men/19.jpg" class="img-circle">
                                      </a>
                                  </div>
                                <div class="col-sm-12">
                                    <table class="info col-sm-3 col-sm-offset-1">
                                        <tr>  <td class="padding-right text-right"> 2415</td>   <td class="bold text-left">Abonnés</td></tr>
                                        <tr>  <td class="padding-right text-right"> 98594</td>  <td class="bold text-left">Tweets </td> </tr>
                                        <tr>  <td class="padding-right text-right"> 2510</td>   <td class="bold text-left">Amis</td>    </tr>
                                        <tr>  <td class="padding-right text-right"> 83</td>     <td class="bold text-left">Listé </td>  </tr>
                                    </table>
                                </div>
							  </div>
                              <a class="vendor" href=""> Google</a>
                                
                               <div class="col-sm-12 action">
                                   <span>Was this relevant? </span><br>
                                   <a href="">
                                    <i class="fa fa-check fa-2x"></i><div class="space-5"></div>
                                   </a>
                                   <a  href="">
                                    <i class="fa fa-times fa-2x"></i><div class="space-5"></div>
                                   </a>
                                   <a  href="">
                                    <i class="fa fa-reply fa-2x"></i><div class="space-5"></div>
                                   </a>
                               </div>
							</div>

                            <div class="row divider">    
							   <div class="col-sm-12"><hr></div>
							</div>		
							<!--/stories-->
                            <?php  } ?>
							
						</div><!-- /col-9 -->
					</div><!-- /padding -->
				</div>
                <!-- /Left Panel -->
                
                <!-- Right panel  -->
				<div class="column main col-sm-6">
					<div class="padding">
						<div class="full col-sm-9">
						  
							<!-- content -->
							
							<div class="col-sm-12" id="featured">   
							  <div class="page-header text-muted">
							  Autres localisation
							  </div> 
							</div>
							
                            <?php for($i=0;$i<20;$i++) { ?>
							<!--stories-->
							<div class="row">    
							  <div class="col-sm-9">
								<h4><span class="glyphicon glyphicon-map-marker"></span> Paris</h4>
								<span class="font-large">
                                    Eau : plus de coupures, mais débit réduit: SOSconso. L'interdiction ou pas des 
                                    coupures d'eau a donné lieu à u... 
                                    <a target="_blank" href="http://t.co/8pNjFbgAuS">http://t.co/8pNjFbgAuS</a>
                                  </span>
                                                                  
                                <h4><small class="text-muted"> 18:20 - 20 Avril 2015 </small></h4>
							  </div>
							  <div class="col-sm-3">
                                  <div class="col-sm-12">
                                      <a href="#" class="col-sm-1 col-sm-offset-3">
                                          <img src="http://api.randomuser.me/portraits/thumb/men/19.jpg" class="img-circle">
                                      </a>
                                  </div>
                                <div class="col-sm-12">
                                    <table class="info col-sm-3 col-sm-offset-1">
                                        <tr>  <td class="padding-right text-right"> 2415</td>   <td class="bold text-left">Abonnés</td></tr>
                                        <tr>  <td class="padding-right text-right"> 98594</td>  <td class="bold text-left">Tweets </td> </tr>
                                        <tr>  <td class="padding-right text-right"> 2510</td>   <td class="bold text-left">Amis</td>    </tr>
                                        <tr>  <td class="padding-right text-right"> 83</td>     <td class="bold text-left">Listé </td>  </tr>
                                    </table>
                                </div>
							  </div>
                              <a class="vendor" href=""> Google</a>
                                
                               <div class="col-sm-12 action">
                                   <span>Was this relevant? </span><br>
                                   <a href="">
                                    <i class="fa fa-check fa-2x"></i><div class="space-5"></div>
                                   </a>
                                   <a  href="">
                                    <i class="fa fa-times fa-2x"></i><div class="space-5"></div>
                                   </a>
                                   <a  href="">
                                    <i class="fa fa-reply fa-2x"></i><div class="space-5"></div>
                                   </a>
                               </div>
							</div>

                            <div class="row divider">    
							   <div class="col-sm-12"><hr></div>
							</div>		
							<!--/stories-->
                            <?php  } ?>
                            
						</div><!-- /col-9 -->
					</div><!-- /padding -->
				</div>
				<!-- /Right Panel -->
			  
			</div>
		</div>
	</div>
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>